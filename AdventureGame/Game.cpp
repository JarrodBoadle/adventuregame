#include <iostream>
#include "Game.h"
#include <string>
#include <climits>
using namespace std;


Game::Game()
{
}

Game::~Game()
{
}

void Game::run()
{
	playerSelect();

	while (!gameOver)
	{
		update();
	}
	
}

void Game::update()
{
	cout << "Update " << endl;
	gameOver = true;
}

void Game::playerType()
{
}

void Game::playerSelect()
{
	//this is where you select player type
	//get the name
	std::string name;
	cout << "What is your name?";
	cin >> name;
	cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	bool done = false;

	while (!done)
	{
		//get player type
		string playerType;
		cout << "Select player type? \n";
		cout << "1. Warrior\n2. Thief\n>>";
		cin >> playerType;
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

		if (playerType == "warrior")
		{
			//create a warrior
			player = Warrior(name);
			cout << player.getDescription() << endl;
			done = true;
		}
		else if (playerType == "thief")
		{
			//create a thief
			player = Theft(name);
			cout << player.getDescription() << endl;
			done = true;
		}
	}

	
}
