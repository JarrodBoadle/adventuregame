#pragma once
#include "Player.h"
#include <string>

class Warrior :
	public Player
{
public:
	Warrior();
	Warrior(std::string name);
	~Warrior();

};

