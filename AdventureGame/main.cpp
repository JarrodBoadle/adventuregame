#include <iostream>
#include "Game.h"
using namespace std;

int main()
{
	//create a game object
	Game game;
	//call the run function
	game.run();

	system("pause");
	return 0;
}