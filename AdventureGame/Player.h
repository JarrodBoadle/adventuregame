#pragma once
#include <string>
#include <vector>
class Player
{
public:
	Player();
	Player(std::string);
	~Player();
	std::vector<std::string>inventory;

	void setName(std::string);
	void setDescription(std::string);
	void setHealth(int);
	void setAttack(int);
	void setLevel(int);

	std::string getName();
	std::string getDescription();
	int getHealth();
	int getAttack();
	int getLevel();

private:
	std::string name;
	std::string description;
	int health;
	int attack;
	int level;

	//weapon


};

