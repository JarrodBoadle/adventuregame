#pragma once
#include <string>
#include "Warrior.h"
#include "Theft.h"

class Game
{
public:
	Game();
	~Game();

	void run();
	void update();

	Player player;

	void playerType();

private:
	void playerSelect();
	bool gameOver = false;
};

